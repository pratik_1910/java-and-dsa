// Rotate Array

import java.io.*;

class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int arr[]={1,2,3,4,5};
		int k=Integer.parseInt(br.readLine());

		int j=1;
		while(j<=k){
			int temp=arr[0];
			for(int i=0;i<arr.length-1;i++){
				arr[i]=arr[i+1];
			}
			arr[arr.length-1]=temp;
			j++;
		}
		
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
	}
}


