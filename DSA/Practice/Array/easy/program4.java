// count pairs with given sum

import java.io.*;

class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		int arr[]={1,5,7,1};
		//int arr[]={1,1,1,1};
		int k=Integer.parseInt(br.readLine());

		int count=0;
		for(int i=0;i<arr.length;i++){
			for(int j=i+1;j<arr.length;j++){
				if((arr[i]+arr[j])==k){
					count++;
				}
			}
		}
		
		System.out.println(count);
	}
}

			
