// Product array Pyzzle

class ArrayDemo{
	public static void main(String[] args){
		int arr[]={10,3,5,6,2};
		//int arr[]={12,0};

		for(int i=0;i<arr.length;i++){
			int prod=1;
			for(int j=0;j<arr.length;j++){
				if(i==j){
					continue;
				}
				prod =prod*arr[j];
			}
			System.out.print(prod+"   ");
		}
	}
}
