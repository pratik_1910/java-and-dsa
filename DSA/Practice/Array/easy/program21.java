// find the first non repeating element from the array
// consider the array consists only positive and negative elements and not consisting zero

class ArrayDemo{
	public static void main(String[] args){
		int arr[]={-1,2,-1,3,2};

		for(int i=0;i<arr.length;i++){
			int count=0;
			for(int j=0;j<arr.length;j++){
				if(arr[i]==arr[j]){
					count++;
				}
			}
			if(count==1){
				System.out.println(arr[i]);
				return;
			}
		}
		System.out.println("0");
	}
}



