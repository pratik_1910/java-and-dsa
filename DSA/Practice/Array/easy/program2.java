//sort the array of 0's , 1's and 2's in assending order

class ArrayDemo{
	public static void main(String[] args){
		int arr[]={0,2,1,2,0};
		//int arr[]={0,1,0};

		int countZero=0,countOne=0,countTwo=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]==0){
				countZero++;
			}else if(arr[i]==1){
				countOne++;
			}else{
				countTwo++;
			}
		}
		for(int i=0;i<countZero;i++){
			System.out.print("0  ");
		}
		for(int i=countZero ; i<countZero+countOne ; i++){
			System.out.print("1  ");
		}
		for(int i=countZero+countOne ; i<countZero+countOne+countTwo ; i++){
			System.out.print("2  ");
		}
	}
}

