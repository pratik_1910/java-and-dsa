// Numer of occurence

import java.io.*;

class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int arr[]={1,1,2,2,2,2,3};
		int k=Integer.parseInt(br.readLine());

		int count=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]==k){
				count++;
				if(arr[i+1]!=k){
					break;
				}
			}
		}
		System.out.println(count);
	}
}


