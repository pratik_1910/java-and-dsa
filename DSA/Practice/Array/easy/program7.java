// check if array is sorted
import java.util.Arrays;

class ArrayDemo{
	public static void main(String[] args){
		int arr[]={10,20,30,40,50};
		//int arr[]={90,80,100,70,40,30};
		int arr1[]=new int[arr.length];

		for(int i=0;i<arr.length;i++){
			arr1[i]=arr[i];
		}

		Arrays.sort(arr);
		
		for(int i=0;i<arr.length;i++){
			if(arr[i]!=arr1[i]){
				System.out.println("0");
				return;
			}
		}
		System.out.println("1");

	}
}

