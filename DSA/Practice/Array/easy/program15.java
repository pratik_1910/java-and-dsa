// union of two arrays

class ArrayDemo{
	public static void main(String[] args){
		int arr1[]={1,2,3,4,5};
		int arr2[]={1,2,3};
		//int arr1[]={2,2,3,4,5};
		//int arr2[]={1,1,2,3,4};
		boolean arr3[]=new boolean[arr1.length+arr2.length];

		int j=0;
		for(int i=0;i<arr1.length;i++){
			if(!arr3[i]){
				System.out.print(arr1[i]+"  ");
				arr3[j]=true;
				j++;
			}
		}
		for(int i=0;i<arr2.length;i++){
			if(!arr3[i]){
				System.out.print(arr2[i]+"  ");
				arr3[j]=true;
				j++;				
			}
		}

	}
}
				
