// find all pairs with given sum from two arrays

import java.io.*;

class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		int arr1[]={1,2,4,5,7};
		int arr2[]={5,6,3,4,8};
		//int arr1[]={-1,-2,4,-6,5,7};
		//int arr2[]={6,3,4,0};
		int sum = Integer.parseInt(br.readLine());

		for(int i=0;i<arr1.length;i++){
			for(int j=0;j<arr2.length;j++){
				if(arr1[i]+arr2[j]==sum){
					System.out.println(arr1[i]+"  "+arr2[j]);
				}
			}
		}
	}
}

