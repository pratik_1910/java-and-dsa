// Alternate positive and negative number

class ArrayDemo{
	public static void main(String[] args){
		int arr[]={9,4,-2,-1,5,0,-5,-3,2};
		//int arr[]={-5,-2,5,2,4,7,1,8,0,-8};
		int arr1[]=new int[arr.length];
		int arr2[]=new int[arr.length];

		int j=0,k=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]>=0){
				arr1[j]=arr[i];
				j++;
			}else{
				arr2[k]=arr[i];
				k++;
			}
		}

		for(int i=0;i<arr.length;i++){
			if(i<j){
				System.out.print(arr1[i]+"  ");
			}
			if(i<k){
				System.out.print(arr2[i]+"  ");
			}
		}
	}
}


