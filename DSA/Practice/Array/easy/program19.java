// element in an array with left side smaller and right side greater

class ArrayDemo{
	public static void main(String[] args){
		int arr[]={4,2,5,7};
		//int arr[]={11,9,12};

		int count=0;
		for(int i=0;i<arr.length;i++){
			if((i!=0) && (i!=arr.length-1)){
				if((arr[i]>arr[i-1]) && (arr[i]<arr[i+1])){
					System.out.print(arr[i]+"  ");
					count++;
				}
			}
		}
		if(count==0){
			System.out.println("-1");
		}
	}
}
