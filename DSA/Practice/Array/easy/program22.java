// leftmost and rightmost index

import java.io.*;

class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int arr[]={1,3,5,5,5,5,67,123,125};
		//int arr[]={1,3,5,5,5,5,7,123,125};
		int k=Integer.parseInt(br.readLine());

		int start=-1,end=-1;
		for(int i=0;i<arr.length;i++){
			if((arr[i]==k) && (arr[i-1]!=k)){
				start=i;
			}
			if((arr[i]==k) && (arr[i+1])!=k){
				end=i;
			}
		}
		System.out.println(start+"  "+end);
	}
}




