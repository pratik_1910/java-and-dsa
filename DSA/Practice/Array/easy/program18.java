// Move all zero's to the end of the array

class ArrayDemo{
	public static void main(String[] args){
		
		int arr[]={3,5,0,0,4};

		int temp=0;
		for(int i=0;i<arr.length-1;i++){
			if(arr[i]==0){
				temp=arr[i];
				arr[i]=arr[i+1];
				arr[i+1]=temp;
			}
		}
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+"  ");
		}
	}
}
