// find the repeating numbers from an array

class ArrayDemo{
	public static void main(String[] args){
		
		int arr[]={0,3,1,2};
		//int arr[]={2,3,1,2,3};
		boolean arr1[]=new boolean[arr.length];

		int count=0,r=0;
		for(int i=0;i<arr.length;i++){
			if(!arr1[i]){
				count=1;
				for(int j=i+1;j<arr.length;j++){
					if(arr[i]==arr[j]){
						count++;
						arr1[j]=true;
					}
				}
				if(count>1){
					System.out.print(arr[i] +"  ");
					r++;
				}
			}
		}
		if(r==0){
			System.out.println("-1");
		}
	}
}

