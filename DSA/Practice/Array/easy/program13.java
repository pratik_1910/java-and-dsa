// minimum distance between two numbers

import java.io.*;

class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		int arr[]={1,2,3,2};
		int A=Integer.parseInt(br.readLine());
		int B=Integer.parseInt(br.readLine());

		int x1=-1,x2=-1;
		for(int i=0;i<arr.length;i++){
			if(arr[i]==A){
				x1=i;
			}
			if(arr[i]==B){
				x2=i;
				break;
			}
		}
		System.out.println(x2-x1);
	}
}
