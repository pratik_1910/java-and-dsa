// Frequencies of limited range elements

import java.io.*;
class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		int arr[]={2,3,2,3,5};
		//int arr[]={3,3,3,3};
		int k=Integer.parseInt(br.readLine());

		for(int i=1;i<=k;i++){
			int count=0;
			for(int j=0;j<arr.length;j++){
				if(arr[j]==i){
					count++;
				}
			}
			System.out.print(count+"  ");
		}
	}
}


