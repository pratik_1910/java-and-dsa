// Bitonic point : in array the elements may increase in ascending order and then maybe decreasing strictly

class ArrayDemo{
	public static void main(String[] args){
		int arr[]={1,15,25,45,42,21,17,12,11};
		//int arr[]={1,45,47,50,5};

		for(int i=0;i<arr.length-1;i++){
			if(arr[i]>arr[i+1]){
				System.out.println(arr[i]);
				return;
			}
		}
	}
}

