// Fibonacci in the Array

class ArrayDemo{
	public static void main(String[] args){
		int arr[]={4,2,8,5,20,1,40,13,23};
		
		// if n for  (5*n*n - 4)  or  (5*n*n + 4) is a perfect square the it is an fibonacci series element
		
	 	boolean perfectSquare(int n){
		
			for(int i=1;i<=n/2;i++){
				if(i*i == n){
					return true;
				}
			}
			return false;
		}

		int count=0;
		for(int i=0;i<arr.length;i++){
			int x=((5*arr[i]*arr[i])-4);
			int y=((5*arr[i]*arr[i])+4);
			boolean n1=perfectSquare(x);
			boolean n2=perfectSquare(y);
			if(n1 || n2){
				count++;
			}
		}
		System.out.println(count);
	}
}
			

