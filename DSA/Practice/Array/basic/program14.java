// maximum repeating number from an array elements

class ArrayDemo{
	public static void main(String[] args){
		int arr[]={2,2,1,2};
		boolean arr1[]=new boolean[arr.length];

		int max =0,count=0,num=0;
		for(int i=0;i<arr.length;i++){
			if(!arr1[i]){
				count=1;
				for(int j=i+1;j<arr.length;j++){
					if(arr[i]==arr[j]){
						count++;
						arr1[j]=true;
					}
				}
				if(max<count){
					max=count;
					num=arr[i];
				}
			}
		}
		System.out.println(num+" is the most repeating element in an array");
		//System.out.println(max);
	}
}


