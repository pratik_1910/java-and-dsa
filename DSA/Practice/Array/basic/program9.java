//remove element from an array at specific index

import java.io.*;

class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int arr[]={1,2,3,4,5};

		int index=Integer.parseInt(br.readLine());

		if((index>arr.length) || (index<0)){
			System.out.println("Enter the correct index");
		}else{
			for(int i=index;i<(arr.length-1);i++){
				arr[i]=arr[i+1];
			}
		}
		System.out.print("{");
		for(int i=0;i<(arr.length-1);i++){
			System.out.print(arr[i]+"  ");
		}
		System.out.print("}");
	}
}





