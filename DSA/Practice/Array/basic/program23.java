// find the smallest and second smallest element in an array

import java.util.Arrays;
class ArrayDemo{
	public static void main(String[] args){
		int arr[]={2,4,3,5,6};
		//int arr[]={1,2,1,3,6,7};
		
		/*using Arrays
		 Arrays.sort(arr);
		System.out.println(arr[0]+ "   "+arr[1]);
		*/

		int min1=arr[0];
		for(int i=0;i<arr.length;i++){
			if(min1>arr[i]){
				min1=arr[i];
			}
		}
		int min2=min1;
		for(int i=0;i<arr.length;i++){
			if((min2>arr[i])||(min1==min2)){
				min2=arr[i];
			}
		}
		System.out.println(min1+"  "+min2);
	}
}

		
