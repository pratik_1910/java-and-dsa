// Immediate Smaller Element

class ArrayDemo{
	public static void main(String[] args){
		int arr[]={4,2,1,5,3};
		//int arr[]={5,6,2,3,1,7};

		for(int i=0;i<arr.length-1;i++){
			if(arr[i]>arr[i+1]){
				System.out.print(arr[i+1]+"  ");
			}else{
				System.out.print("-1  ");
			}
		}
		System.out.println("-1");
	}
}
