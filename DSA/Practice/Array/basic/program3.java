// Find out the largest element in an array

class ArrayDemo{
	public static void main(String[] args){
		int arr[]={1,8,7,56,90};
		int largest =arr[0];

		for(int i=0;i<arr.length;i++){
			if(largest < arr[i]){
				largest = arr[i];
			}
		}
		
		System.out.println("Thelargest element in an given array is : "+largest);
	}
}
