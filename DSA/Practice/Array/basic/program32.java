// maximize sum (arr[i]*i) of an array

import java.util.*;

class ArrayDemo{
	public static void main(String[] args){
		int arr[]={5,3,2,4,1};
		//int arr[]={1,2,3};
		Arrays.sort(arr);
		int sum =0;

		for(int i=0;i<arr.length;i++){
			sum=sum+(arr[i]*i);
		}
		System.out.println(sum);
	}
}
