// Array contains equal number of positive and negative elements. 
// so, print the positive and negative elements such that each positive element is followed by a negative element

class ArrayDemo{
	public static void main(String[] args){
		int arr[]={-1,2,-3,4,-5,6};
		//int arr[]={-3,2,-4,1};
		int arr1[]=new int[arr.length/2];
		int arr2[]=new int[arr.length/2];
		
		int j=0,k=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]>=0){
				arr1[j]=arr[i];
				j++;
			}else{
				arr2[k]=arr[i];
				k++;
			}
		}
		
		for(int i=0;i<arr.length/2;i++){
			System.out.println(arr1[i]);
			System.out.println(arr2[i]);
		}
	}
}

