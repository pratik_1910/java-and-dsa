// find a peak element which is not smaller than its neighbours

class ArrayDemo{
	public static void main(String[] args){
		int arr[]={5,10,20,15};
		//int arr[]={10,20,15,2,23,90,67};

		for(int i=0;i<arr.length;i++){
			if(i==0){
				if(arr[i]>arr[i+1]){
					System.out.print(arr[i]+"  ");
				}
			}else if(i==arr.length){
				if(arr[i]>arr[i-1]){
					System.out.print(arr[i]+"  ");
				}
			}else{
				if((arr[i-1]<arr[i]) && (arr[i]>arr[i+1])){
					System.out.println(arr[i]+"  ");
				}
			}
		}
	}
}
			
