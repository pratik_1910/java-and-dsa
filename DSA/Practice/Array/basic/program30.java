// find out the closest number

import java.io.*;

class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		int arr[]={1,3,6,7};
		int num=Integer.parseInt(br.readLine());
		int floor=-1,ceil=-1;

		for(int i=0;i<arr.length;i++){
			if(i<arr.length-1){
				if((arr[i]<num) && (arr[i+1]>num)){
					floor=arr[i];
				}
			}
			if(i>0){
				if((arr[i]>num) && (arr[i-1]<num)){
					ceil=arr[i];
				}
			}
			if(num>arr[arr.length-1]){
				System.out.println(arr[arr.length-1]);
				return;
			}
			if(num<arr[0]){
				System.out.println(arr[0]);
				return;
			}
		}
		
		if((num-floor)<(ceil-num)){
			System.out.println(floor);
		}else{
			System.out.println(ceil);
		}
	}
}


