// find the floor and ceil values of the element in an array
// floor : greatest value which is samller or equal to element
// ceil : smallest value which is greater than or equal to element

import java.util.Arrays;
import java.io.*;

class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		int arr[]={5,6,8,9,6,5,5,6};
		//int arr[]={5,6,8,9,6,5,5,6};
		int num=Integer.parseInt(br.readLine());

		Arrays.sort(arr);
		int floor=-1,ceil=-1;
		
		for(int i=0;i<arr.length;i++){
			if(i!=arr.length-1){
				if((arr[i]<num) && (arr[i+1]>=num)){
					floor=arr[i];
				}
			}
			if(num>arr[arr.length-1]){
				floor=arr[arr.length-1];
			}
			if(i!=0){
				if((arr[i]>num) && (arr[i-1]<=num)){
					ceil=arr[i];
				}
			}
			if(num<arr[0]){
				ceil=arr[0];
			}
		}
		System.out.println(floor +"   "+ceil);
	}
}
		






