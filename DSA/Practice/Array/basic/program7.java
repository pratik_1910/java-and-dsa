// form the largest number from the elements of an array

import java.util.Arrays;

class ArrayDemo{
	public static void main(String[] args){
		int arr[]={9,0,1,3,0};
		Arrays.sort(arr);
		int num=0;

		for(int i=arr.length-1;i>=0;i--){
			num = (num*10)+arr[i];
		}
		System.out.println(num);
	}
}
