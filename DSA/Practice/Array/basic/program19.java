// find common elements in three sorted arrays

import java.io.*;

class ArrayDemo{
	public static void main(String[] args)throws IOException{
		int arr1[]={1,5,10,20,40,80};
		int arr2[]={6,7,20,80,100};
		int arr3[]={3,4,15,20,30,70,80,120};

		/*another test case!
		  int arr1[]={1,5,5};
		  int arr2[]={3,4,5,5,10};
		  int arr3[]={5,5,10,20};*/
		 

		for(int i=0;i<arr1.length;i++){
			for(int j=0;j<arr2.length;j++){
				if(arr1[i]==arr2[j]){
					for(int k=0;k<arr3.length;k++){
						if(arr1[i]==arr3[k]){
							System.out.print(arr1[i]+"  ");
						}
					}
				}
			}
		}
		System.out.println();
	}
}
