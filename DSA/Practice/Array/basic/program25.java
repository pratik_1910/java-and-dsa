// maximum product of two numbers in an array

import java.util.*;
class ArrayDemo{
	public static void main(String[] args){
		int arr[]={1,4,3,6,7,0};
		//int arr[]={1,100,42,4,23};
		//Arrays.sort(arr);
		
		for(int i=0;i<arr.length;i++){
			for(int j=i+1;j<arr.length;j++){
				int temp=0;
				if(arr[i]>arr[j]){
					temp=arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}
			}
		}


		System.out.println(arr[arr.length-1] * arr[arr.length-2]);
	}
}

