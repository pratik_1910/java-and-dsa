//first and last occurrence of an element in an array 

import java.io.*;
class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int arr[]={1,3,3,4};
		int num = Integer.parseInt(br.readLine());
		int first=-1,last=-1;

		for(int i=0;i<arr.length;i++){
			if(num!=arr[i]){
				continue;
			}
			if(first==-1){
				first = i;
			}
			last = i;
		}
		if(first!=-1){
			System.out.println("Thre first occurrence is : "+ first +" and the last occurrence is : "+ last);
		}else{
			System.out.println("The element is not found");
		}
	}
}



