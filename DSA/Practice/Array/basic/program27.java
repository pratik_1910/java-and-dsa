// count pair sum

import java.io.*;

class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int arr1[]={1,3,5,7};
		int arr2[]={2,3,5,8};
		int sum=Integer.parseInt(br.readLine());
		int count=0;

		for(int i=0;i<arr1.length;i++){
			for(int j=0;j<arr2.length;j++){
				if((arr1[i]+arr2[j])==sum){
					count++;
				}
			}
		}
		System.out.println(count);
	}
}


