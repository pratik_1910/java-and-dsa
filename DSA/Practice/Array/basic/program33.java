// multiply the left and right array sum

class ArrayDemo{
	public static void main(String[] args){
		int arr[]={1,2,3,4};
		//int arr[]={1,2};
		int sum1=0,sum2=0;

		for(int i=0;i<arr.length/2;i++){
			sum1=sum1+arr[i];
		}

		for(int i=arr.length/2;i<arr.length;i++){
			sum2=sum2+arr[i];
		}

		System.out.println(sum1*sum2);
	}
}
