// first element to occur k times
import java.io.*;
class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int arr[]={1,7,4,3,4,8,7};
		int n = Integer.parseInt(br.readLine());
		boolean arr1[]=new boolean[arr.length];
		
		int count=0;
		for(int i=0;i<arr.length;i++){
			if(!arr1[i]){
				count=1;
				for(int j=i+1;j<arr.length;j++){
					if(arr[i]==arr[j]){
						count++;
						arr1[j]=true;
					}
				}
			}
			if(count==n){
				System.out.println(arr[i]);
				break;
			}
		}
	}
}

