// sum of distinct elements in an array

class ArrayDemo{
	public static void main(String[] args){
		int arr[]={1,2,3,4,5};
		boolean arr1[]=new boolean[arr.length];
		int sum=0,count=0;
		for(int i=0;i<arr.length;i++){
			if(!arr1[i]){
				count=1;
				for(int j=i+1;j<arr.length;j++){
					if(arr[i]==arr[j]){
						count++;
						arr1[j]=true;
					}
				}
				if(count==1){
					sum=sum+arr[i];
				}
			}
		}
		System.out.println("The sum of the distinct elements in an given array is : "+sum);
	}
}
