// check if pair with given sum exists in Array(two sum)

import java.io.*;
class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int arr[]={0,-1,2,-3,1};
		int sum = Integer.parseInt(br.readLine());

		for(int i=0;i<arr.length;i++){
			for(int j=i+1;j<arr.length;j++){
				if((arr[i]+arr[j])==sum){
					System.out.println("Yes");
					return;
				}
			}
		}
		System.out.println("No");
	}
}
