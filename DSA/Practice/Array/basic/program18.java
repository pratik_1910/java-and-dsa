// find the subarray from the given array with a given sum

import java.io.*;

class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int arr[]={1,40,20,3,10,5};
		int sum=Integer.parseInt(br.readLine());
		
		int curr_sum=0;
		for(int i=0;i<arr.length;i++){
			curr_sum=arr[i];
			for(int j=i+1;j<arr.length;j++){
				if(curr_sum==sum){
					int k=j-1;
					System.out.println("sum found between indexes "+i+" and "+k);
					return;
				}
				if((curr_sum>sum) || (j==arr.length)){
					break;
				}
				curr_sum=curr_sum + arr[j];
			}
		}
		System.out.println("No subarray found");
	}
}


