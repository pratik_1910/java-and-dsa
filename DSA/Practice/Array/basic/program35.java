// minimum product of k integers

import java.io.*;
import java.util.Arrays;

class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int arr[]={1,2,3,4,5};
		//int arr[]={9,10,8};
		int k=Integer.parseInt(br.readLine());
		Arrays.sort(arr);
		int prod=1;

		for(int i=0;i<k;i++){
			prod=prod*arr[i];
		}
		System.out.println(prod);
	}
}



