// count the number of elements between two given elements in an array

import java.io.*;

class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int arr[]={4,2,1,10,6};
		int n1=Integer.parseInt(br.readLine());
		int n2=Integer.parseInt(br.readLine());

		int count=0,ind1=-1,ind2=-1;
		for(int i=0;i<arr.length;i++){
			if(arr[i]==n1){
				ind1=i;
			}
			if(arr[i]==n2){
				ind2=i;
			}
		}

		System.out.println(ind2-ind1-1);
	}
}



