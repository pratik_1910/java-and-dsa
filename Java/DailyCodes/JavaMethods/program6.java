// passing values to a function

import java.util.*;
class Demo{
	static void add(int x,int y){
		int ans=x+y;
		System.out.println("The addition of "+x+" and "+y+"is : "+ans);
	}

	static void sub(int x, int y){
		int ans=x-y;
		System.out.println("The substraction of "+x+" and"+y+" is : "+ans);
	}

	static void mult(int x,int y){
		int ans=x*y;
		System.out.println("The multiplication of "+x+" and"+y+" is : "+ans);
	}

	static void div(int x,int y){
		int ans=x/y;
		System.out.println("The division of "+x+" and"+y+" is : "+ans);
	}

	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the two integer numbers");
		int a=sc.nextInt();
		int b=sc.nextInt();

		add(a,b);
		sub(a,b);
		mult(a,b);
		div(a,b);
	}}

