class MyThread extends Thread{
	MyThread(ThreadGroup tg,String str){
		super(tg,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
		try{
			Thread.sleep(5000);
		}catch(InterruptedException ie){
			System.out.println(ie.toString());
		}
	}
}

class ThreadDemo{
	public static void main(String[] args)throws InterruptedException{
		ThreadGroup pThreadGp=new ThreadGroup("India");
		MyThread t1=new MyThread(pThreadGp,"Maha");
		MyThread t2=new MyThread(pThreadGp,"Goa");
		t1.start();
		t2.start();

		ThreadGroup cThreadGp=new ThreadGroup(pThreadGp,"Pak");
		MyThread t3=new MyThread(cThreadGp,"Karachi");
		MyThread t4=new MyThread(cThreadGp,"Lahore");
		t3.start();
		t4.start();

		ThreadGroup cThreadGp2=new ThreadGroup(pThreadGp,"Bangla");
		MyThread t5=new MyThread(cThreadGp2,"Dhaka");
		MyThread t6=new MyThread(cThreadGp2,"Mirpur");
		t5.start();
		t6.start();

		System.out.println(pThreadGp.activeCount());
		System.out.println(pThreadGp.activeGroupCount());
	}
}
