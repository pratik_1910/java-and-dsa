class MyThread extends Thread{
	MyThread(ThreadGroup tg,String str){
		super(tg,str);
	}

	public void run(){
		System.out.println(Thread.currentThread());
	}
}

class ThreadDemo{
	public static void main(String[] args){
		ThreadGroup pthreadGp=new ThreadGroup("Core2web");
		MyThread obj=new MyThread(pthreadGp,"C");
		MyThread obj1=new MyThread(pthreadGp,"Java");
		MyThread obj2=new MyThread(pthreadGp,"python");

		obj.start();
		obj1.start();
		obj2.start();

		ThreadGroup cthreadGp=new ThreadGroup(pthreadGp,"Incubator");
		MyThread obj4=new MyThread(cthreadGp,"Flutter");
		MyThread obj5=new MyThread(cthreadGp,"ReactJS");
		MyThread obj6=new MyThread(cthreadGp,"Springboot");
		
		obj4.start();
		obj5.start();
		obj6.start();

	}
}
