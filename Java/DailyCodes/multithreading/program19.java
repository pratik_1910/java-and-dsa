class MyThread extends Thread{
	MyThread(ThreadGroup tg,String str){
		super(tg,str);
	}
	public void run(){
		System.out.println(getName());
		System.out.println(Thread.currentThread().getThreadGroup());
	}
}

class ThreadDemo{
	public static void main(String[] args){
		ThreadGroup pThreadGp=new ThreadGroup("Core2Web");
		MyThread obj1=new MyThread(pThreadGp,"C");
		MyThread obj2=new MyThread(pThreadGp,"C++");
		obj1.start();
		obj2.start();
		ThreadGroup cThreadGp=new ThreadGroup(pThreadGp,"Incubator");
		MyThread obj3=new MyThread(cThreadGp,"Flutter");
		MyThread obj4=new MyThread(cThreadGp,"ReadcJs");
		obj3.start();
		obj4.start();
		System.out.println(pThreadGp.activeCount());
		System.out.println(pThreadGp.activeGroupCount());
	}
}
