// ThreadPool : newFixedThreadPool()

import java.util.concurrent.*;

class MyThread implements Runnable{
	int num;
	MyThread(int num){
		this.num=num;
	}
	public void run(){
		System.out.println(Thread.currentThread()+" start Thread "+num);
		dailyTask();
		System.out.println(Thread.currentThread()+" End Thread "+num);
	}
	void dailyTask(){
		try{
	        	Thread.sleep(5000);
		}catch(InterruptedException ie){
		}
	}
}

class ThreadDemo{
	public static void main(String[] args){
		ExecutorService ser = Executors.newFixedThreadPool(3);
		for(int i=1;i<10;i++){
			MyThread obj = new MyThread(i);
			ser.execute(obj);
		}
		ser.shutdown();
	}
}


