class Demo{
	static{
		System.out.println("static Block1");
	}
	public static void main(String[] args){
		System.out.println("In Demo main");
	}
}

class Client{
	static{
		System.out.println("In static block2");
	}

	public static void main(String[] args){
		System.out.println("In Client main");
	}
	static{
		System.out.println("static Block3");
	}
}



