// Divisible by 3  , 5 and both

class Div{
	public static void main(String[] args){
		int x = 5;

		if(x %3==0 && x%5 == 0){
			System.out.println("Fizz - Buzz");
		}else if( x%3 == 0){
			System.out.println("Fizz");
		}else if(x%5 == 0){
			System.out.println("Buzz");
		}else{
			System.out.println("Not divisible by both");
		}
	}
}
