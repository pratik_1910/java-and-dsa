//Multiplication of the digits

class Demo{
	public static void main(String[] args){
		int N=6532;
		int rem=0,Mult = 1;
		while(N!=0){
			rem=N%10;
			Mult=Mult*rem;
			N=N/10;
		}
		System.out.println("Multiplication of the digits ="+Mult);
	}
}
