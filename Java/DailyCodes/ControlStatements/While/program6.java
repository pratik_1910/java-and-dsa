//Print sum of the digits

class Demo{
	public static void main(String[] args){
		int N=6531;
		int sum=0,rem=0;
		while(N!=0){
			rem=N%10;
			sum=sum+rem;
			N=N/10;
		}
		System.out.println("Sum of the digits = "+sum);
	}
}
