// multiples of 4

class Demo{
	public static void main(String[] args){
		int i=1;
		int n= 20;

		while(i<=n){
			if(i%4==0){
				System.out.println(i);
			}
			i++;
		}
	}
}
