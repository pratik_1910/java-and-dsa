// Take a integer
// Print the perfect square till the N

class Demo{
	public static void main(String[] args){
		int N=30;
		int i=1;
		while(i<=N){
			if(i*i<=N){
				System.out.println(i*i);
			}
			i++;
		}
	}
}
