// Reverse the number

class Demo{
	public static void main(String[] args){
		int N=123;
		int rev=0,rem=0;
		while(N!=0){
			rem=N%10;
			rev=(rev*10)+rem;
			N=N/10;
		}
		System.out.println(rev);
	}
}
