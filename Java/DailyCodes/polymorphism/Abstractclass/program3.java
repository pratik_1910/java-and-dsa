abstract class parent{
	void property(){
		System.out.println("car,flat,gold");
	}
	abstract void occupation();
}
class child extends parent{
	void occupation(){
		System.out.println("Businessman");
	}
}
class Client{
	public static void main(String[] args){
		parent obj=new child();
		obj.property();
		obj.occupation();
	}
}

