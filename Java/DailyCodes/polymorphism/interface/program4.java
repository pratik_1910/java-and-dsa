//  Concrete method in interface

interface Demo1{
	static void m1(){
		System.out.println("Demo1-m1");
	}
}

interface Demo2{
	static void m1(){
		System.out.println("Demo2-m1");
	}
}

interface Demo3 extends Demo1,Demo2{
	
}

class childDemo implements Demo3{
	public static void main(String[] args){
		// Demo1 obj1=new childDemo();
		// obj1.m1();                          // error

		Demo1.m1();
		Demo2.m1();
	}
}


