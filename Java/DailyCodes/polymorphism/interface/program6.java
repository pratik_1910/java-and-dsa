interface Demo1{
	default void fun(){
		System.out.println("In fun Demo1");
	}
}
interface Demo2{
	default void fun(){
		System.out.println("In fun Demo2");
	}
}
class childDemo implements Demo1,Demo2{
	public void fun(){
		System.out.println("In fun childDemo");
	}
}
class Client{
	public static void main(String[] args){
		Demo1 obj1=new childDemo();
		obj1.fun();
		Demo2 obj2=new childDemo();
		obj2.fun();
		childDemo obj3=new childDemo();
		obj3.fun();
	}
}
