interface Demo{
	void gun();
	default void fun(){
		System.out.println("In fun Demo");
	}
}
class Demochild implements Demo{
	public void gun(){
		System.out.println("in gun Demochild");
	}
}
class Client{
	public static void main(String[] args){
		Demo obj1=new Demochild();
		obj1.fun();
		obj1.gun();
	}
}


