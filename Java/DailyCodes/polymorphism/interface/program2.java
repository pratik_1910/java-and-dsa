interface parent{
	void fun();
	void gun();
}
abstract class child implements parent{
	public void gun(){
		System.out.println("In gun");
	}
}
class child1 extends child{
	public void fun(){
		System.out.println("in fun");
	}
}
class Client{
	public static void main(String[] args){
		parent obj=new child1();
		obj.fun();
		obj.gun();

	}
}


