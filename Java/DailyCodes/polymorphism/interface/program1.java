interface parent{
	void fun();
	void gun();
}
class child implements parent{
	public void fun(){
		System.out.println("In fun");
	}
	public void gun(){
		System.out.println("In gun");
	}
}
class Client{
	public static void main(String[] args){
		parent obj=new child();
		obj.fun();
		obj.gun();
	}
}


