import java.util.*;

class StudentCriteriaException extends RuntimeException{
	StudentCriteriaException(String msg){
		super(msg);
	}
}

class PlacementDrive{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter approx. aggrigate marks :");
		int marks=sc.nextInt();
		if(marks<60)
			throw new StudentCriteriaException("The criteria is not fulfilled ");
	}
}
