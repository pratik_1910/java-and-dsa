class parent{
	static int x=10;
	static{
		System.out.println("In parent static block");
	}
	static void access(){
		System.out.println(x);
	}
}
class child extends parent{
	static{
		System.out.println("in child static block");
		System.out.println(x);
		access();
	}
}
class Client{
	public static void main(String[] args){
		System.out.println("in main method");
		child obj=new child();
	}
}
