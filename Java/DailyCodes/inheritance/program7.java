class parent{
	int x=10;
	static int y=20;
	parent(){
		System.out.println("in parent constructor");
	}
}
class child extends parent{
	int x=100;
	static int y=200;
	child(){
		System.out.println("in child constructor");
	}
	void access(){
		System.out.println(super.x);
		System.out.println(super.y);
		System.out.println(x);
		System.out.println(y);
	}
}
class Client{
	public static void main(String[] args){
		child obj=new child();
		obj.access();
	}
}

