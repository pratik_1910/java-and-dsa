class parent{
	parent(){
		System.out.println("in parent constructor ");
	}
	void parentproperty(){
		System.out.println("Gold,Car,Flat");
	}
}
class child extends parent{
	child(){
		System.out.println("In child constructor");
	}
}
class Client{
	public static void main(String[] args){
		child obj=new child();
		obj.parentproperty();
	}
}

