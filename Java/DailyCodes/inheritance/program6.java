class parent{
	int x=10;
	static int y=20;
	static{
		System.out.println("parent staticblock ");
	}
	parent(){
		System.out.println("in parent constructor");
	}
	void methodOne(){
		System.out.println(x);
		System.out.println(y);
	}
	static void methodTwo(){
		System.out.println(y);
	}
}
class child extends parent{
	static{
		System.out.println("in child static block");
	}
	child(){
		System.out.println("in child constructor");
	}
}
class Client{
	public static void main(String[] args){
		child obj=new child();
		obj.methodOne();
		obj.methodTwo();
	}
}

