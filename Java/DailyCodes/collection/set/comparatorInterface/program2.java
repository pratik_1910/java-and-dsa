import java.util.*;

class Movies{
	String mName=null;
	float totColl=0.0f;
	float Imdb=0.0f;

	Movies(String mName,float totColl,float Imdb){
		this.mName = mName;
		this.totColl=totColl;
		this.Imdb=Imdb;
	}

	public String toString(){
		return "{"+mName+":"+totColl+","+Imdb+"}";
	}
}

class SortByName implements Comparator{
	public int compare(Object obj1 , Object obj2){
		return ((Movies)obj1).mName.compareTo(((Movies)obj2).mName);
	}
}

class SortByColl implements Comparator{
	public int compare(Object obj1 , Object obj2){
		return (int) ((((Movies)obj1).totColl) - (((Movies)obj2).totColl));
	}
}

class SortByImdb implements Comparator{
	public int compare(Object obj1,Object obj2){
		return (int) ((((Movies)obj1).Imdb) - (((Movies)obj2).Imdb));
	}
}

class UserListSort{
	public static void main(String[] args){
		ArrayList al=new ArrayList();
		al.add(new Movies("RHTDM",200.0f,8.8f));
		al.add(new Movies("Ved",75.0f,7.5f));
		al.add(new Movies("Sairat",100.0f,8.9f));
		al.add(new Movies("Bajrangi",500.0f,9.9f));

		System.out.println(al);

		Collections.sort(al,new SortByName());
		System.out.println(al);

		Collections.sort(al,new SortByColl());
		System.out.println(al);

		Collections.sort(al,new SortByImdb());
		System.out.println(al);
	}
}
