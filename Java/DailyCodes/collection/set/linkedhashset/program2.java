//LinkedHashSet

import java.util.*;
class CricPlayer{
	int jerNo=0;
	String pName=null;

	CricPlayer(int jerNo,String pName){
		this.jerNo=jerNo;
		this.pName=pName;
	}
	public String toString(){
		return jerNo+" : "+pName;
	}
}

class HashDemo{
	public static void main(String[] args){
		LinkedHashSet lh=new LinkedHashSet();

		lh.add(new CricPlayer(18,"Virat"));
		lh.add(new CricPlayer(7,"MSDhoni"));
		lh.add(new CricPlayer(45,"Rohit"));
		lh.add(new CricPlayer(7,"MSDhoni"));

		System.out.println(lh);
	}
}

