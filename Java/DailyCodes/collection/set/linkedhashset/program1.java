// LinkedHashSet

import java.util.*;

class LinkedHashSetDemo{
	public static void main(String[] args){
		LinkedHashSet lh=new LinkedHashSet();

		lh.add("Rohit");
		lh.add("Virat");
		lh.add("Mahendra");
		lh.add("Surya");
		lh.add("Dipak");
		lh.add("Dinesh");
		lh.add("Mahendra");

		System.out.println(lh);
	}
}
