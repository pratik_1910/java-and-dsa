//TreeSet

import java.util.*;
class TreeSetDemo{
	public static void main(String[] args){
		TreeSet ts=new TreeSet();

		ts.add("Rohit");
		ts.add("Mahendra");
		ts.add("Virat");
		ts.add("Rahul");
		ts.add("Saurav");

		System.out.println(ts);

		System.out.println(ts.first());
		System.out.println(ts.last());
		
	}
}
