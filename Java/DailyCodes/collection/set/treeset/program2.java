import java.util.*;

class Myclass implements Comparable{
	String str=null;
	Myclass(String str){
		this.str=str;
	}
	public int compareTo(Object obj){
		return ((Myclass)obj).str.compareTo(this.str);
	}
	public String toString(){
		return str;
	}
}



class TreeSetDemo{
	public static void main(String[] args){
		TreeSet ts=new TreeSet();

		ts.add(new Myclass("Mahendra"));
		ts.add(new Myclass("Virat"));
		ts.add(new Myclass("Surya"));
		ts.add(new Myclass("Rohit"));

		System.out.println(ts);
	}
}
