// HashSet

import java.util.*;

class HashSetDemo{
	public static void main(String[] args){
		HashSet hs=new HashSet();

		hs.add("Pratik");
		hs.add("Nilesh");
		hs.add("Kunal");
		hs.add("Rutuparn");
		hs.add("Yash");

		System.out.println(hs);
		System.out.println(hs.size());
		System.out.println(hs.isEmpty());
		System.out.println(hs.contains("Kunal"));

		hs.clone();
		System.out.println(hs);
		
		hs.remove("Kunal");
		System.out.println(hs);

		hs.clear();
		System.out.println(hs);
	}
}
