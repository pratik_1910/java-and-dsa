// In Java in Collection linked list have index because it is child of the list(interface)
// 1. E getFirst();
// 2. E getLast();
// 3. E removeFirst();
// 4. E removeLast();
// 5. E addFirst();
// 6. E addLast();


import java.util.*;

class LinkedListDemo{
	public static void main(String[] args){
		LinkedList ll=new LinkedList();
		
		// 1. add()
		ll.add(10);
		ll.add(20);
		ll.add(30);

		System.out.println(ll);

		//2. E addFirst()
		ll.addFirst(1);
		System.out.println(ll);

		//3. E addLast()
		ll.addLast(50);
		System.out.println(ll);

		//4. E removeFirst()
		System.out.println(ll.removeFirst());
		System.out.println(ll);

		//5. E removeLast()
		ll.removeLast();
		System.out.println(ll);

		//6. E getFirst()
		System.out.println(ll.getFirst());

		//7. E getLast()
		System.out.println(ll.getLast());



	}
}

