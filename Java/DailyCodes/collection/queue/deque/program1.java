// Deque
// boolean offerFirst(E);
// boolean offerLast(E);
// E pollFirst();
// E pollLast();
// E peekFirst();
// E peekLast();
// Iterator<E> iterator();
// Iterator<E> descendingIterator();

import java.util.*;

class DequeDemo{
	public static void main(String[] args){
		Deque obj=new ArrayDeque();

		obj.offer(10);
		obj.offer(40);
		obj.offer(20);
		obj.offer(30);
		System.out.println(obj);
		
		obj.offerFirst(50);
		System.out.println(obj);

		obj.offerLast(100);
		System.out.println(obj);

		obj.pollFirst();
		obj.pollLast();
		System.out.println(obj);

		System.out.println(obj.peekFirst());
		System.out.println(obj.peekLast());

		//iterator()
		Iterator itr = obj.iterator();
		while(itr.hasNext()){
			System.out.println(itr.next());
		}
		
		//descendingIterator()	
		Iterator itr2 = obj.descendingIterator();
		while(itr2.hasNext()){
			System.out.println(itr2.next());
		}


	}
}

