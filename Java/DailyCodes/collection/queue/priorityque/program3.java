// use of comparable

import java.util.*;
class Project implements Comparable{
	String pName=null;
	int teamSize=0;
	int duration=0;

	Project(String pName,int teamSize,int duration){
		this.pName=pName;
		this.teamSize=teamSize;
		this.duration=duration;
	}

	public int compareTo(Object obj){
		return this.pName.compareTo(((Project)obj).pName);
	}

	public String toString(){
		return "{"+pName+":"+teamSize+","+duration+"}";
	}
}

class Demo{
	public static void main(String[] args){
		PriorityQueue pq = new PriorityQueue();

		pq.offer(new Project("AAA",5,10));
		pq.offer(new Project("BBB",10,6));
		pq.offer(new Project("CCCC",7,4));
		System.out.println(pq);
	}
}
