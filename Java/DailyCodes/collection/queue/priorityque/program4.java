// use of comparable

import java.util.*;
class Project{
	String pName=null;
	int teamSize=0;
	int duration=0;

	Project(String pName,int teamSize,int duration){
		this.pName=pName;
		this.teamSize=teamSize;
		this.duration=duration;
	}

	public String toString(){
		return "{"+pName+":"+teamSize+","+duration+"}";
	}
}

class SortByDuration implements Comparator{
	public int compare(Object obj1,Object obj2){
		return (((Project)obj1).duration) - (((Project)obj2).duration);
	}
}

class Demo{
	public static void main(String[] args){
		PriorityQueue pq = new PriorityQueue(new SortByDuration());

		pq.offer(new Project("AAA",5,10));
		pq.offer(new Project("BBB",10,6));
		pq.offer(new Project("CCCC",7,4));
		System.out.println(pq);
	}
}
