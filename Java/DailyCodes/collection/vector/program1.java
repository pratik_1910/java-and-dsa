// Vector() [interface]

import java.util.*;

class VectorDemo{
	public static void main(String[] args){
		
		Vector v=new Vector();
		v.addElement(10);
		v.addElement(20);
		v.addElement(30);
		v.addElement(40);

		System.out.println(v);
		System.out.println(v.capacity());
		System.out.println(v.size());
		v.trimToSize();
		System.out.println(v);

		// void ensureCapacity(int)

		v.setSize(2);
		System.out.println(v);

		System.out.println(v.isEmpty());

		System.out.println(v.contains(20));

		v.add(30);
		v.add(40);
		v.add(50);

		System.out.println(v.indexOf(30));
		System.out.println(v.firstElement());
		System.out.println(v.lastElement());
		System.out.println(v.elementAt(4));
		
		v.setElementAt(100,1);
		System.out.println(v);

		v.removeElementAt(2);
		System.out.println(v);

		v.insertElementAt(99,4);
		System.out.println(v);

		v.removeAllElements();
		System.out.println(v);

	}
}
	
