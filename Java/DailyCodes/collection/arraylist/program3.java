import java.util.*;

class CricPlayer{
	int jerNo=0;
	String pName=null;
	CricPlayer(int jerNo,String pName){
		this.jerNo=jerNo;
		this.pName=pName;
	}

	public String toString(){
		return jerNo + " : "+pName;
	}
}

class ArrayListDemo{
	public static void main(String[] args){
		ArrayList al=new ArrayList();

		al.add(new CricPlayer(18,"Virat"));
		al.add(new CricPlayer(7,"MSDhoni"));
		al.add(new CricPlayer(45,"Rohit"));

		System.out.println(al);
	}
}
