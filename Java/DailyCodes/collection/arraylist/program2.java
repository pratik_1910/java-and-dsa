import java.util.*;
class ArrayListDemo extends ArrayList{
	public static void main(String[] args){
		ArrayListDemo al=new ArrayListDemo();
		
		// 1. add(Element)
		al.add(10);
		al.add(20.5f);
		al.add("Pratik");
		System.out.println(al);

		// 2. int size()
		System.out.println("size : "+al.size());

		// 3. boolean contains(object)
		System.out.println(al.contains("Pratik"));
		//System.out.println(al.contains(20));
		
		// 4. int indexOf(object)
		System.out.println(al.indexOf(20.5f));
		//System.out.println(al.indexOf(25));

		al.add(10);
		// 5. int lastIndexOf(object)
		System.out.println(al.lastIndexOf(10));

		System.out.println(al);

		// 6.E get(int)
		System.out.println(al.get(3));

		// 7. E set(int,E)
		System.out.println(al.set(3,"Patil"));
		System.out.println(al);

		// 8. void add(int,E)
		al.add(4,"JSPM");
		System.out.println(al);

		// 9. E remove(int)
		System.out.println(al.remove(4));
		System.out.println(al);

		// 10. boolean remove(object)
		al.remove(20.5f);
		System.out.println(al);

		// 11. boolean addAll(Collection)
		ArrayList al2=new ArrayList();
		al2.add(20);
		al2.add("JSPM");
		al.addAll(al2);
		//System.out.println(al.addAll(al2));
		System.out.println(al);

		// 12. boolean addAll(int,Collection)
		al.addAll(4,al2);
		System.out.println(al);

		// 13. protected void removeRange(int , int)
		al.removeRange(4,6);
		System.out.println(al);

		// 14. java.lang.Object[] toArray()
		Object arr[]=al.toArray();
		for(Object data:arr){
			System.out.print(data+"  ");
		}
		System.out.println();

		// void clear()
		al.clear();
		System.out.println(al);

	}
}
