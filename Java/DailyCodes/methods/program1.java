import java.util.*;

class Demo{
	static int compareStrings(String str1,String str2){
		char arr1[]=str1.toCharArray();
		char arr2[]=str2.toCharArray();
		if(arr1.length == arr2.length){
			for(int i=0;i<arr1.length;i++){
				if(arr1[i] != arr2[i]){
					return(arr1[i]-arr2[i]);
				}
			}
			return 0;
		}
		else{
			return(arr1.length - arr2.length);
		}
	}

	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the first string : ");
		String str1=sc.next();
		System.out.println("Enter the second string : ");
		String str2=sc.next();

		System.out.println(compareStrings(str1,str2));
	}
}

