//Take integer as N and print count of its factors

class Demo{
	public static void main(String[] args){
		int n=20;
		int count=0;
		for(int i=1;i<=n;i++){
			if(n%i==0){
				count++;
			}
		}
		System.out.println("The no of factors = "+count);
	}
}
