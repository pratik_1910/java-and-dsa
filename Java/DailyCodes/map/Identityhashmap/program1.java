// IdentityHashMap allows duplicate keys

import java.util.*;

class IdentityHashDemo{
	public static void main(String[] args){
		IdentityHashMap hm=new IdentityHashMap();

		hm.put(new Integer(10),"Kanha");
		hm.put(new Integer(10),"Rahul");
		hm.put(new Integer(10),"Badhe");

		System.out.println(hm);
	}
}
