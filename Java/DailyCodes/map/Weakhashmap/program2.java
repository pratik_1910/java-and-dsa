import java.util.*;

class Demo{
	String str;
	Demo(String str){
		this.str=str;
	}

	public String toString(){
		return str;
	}

	public void finalize(){
		System.out.println("Notify");
	}
}

class GCDemo{
	public static void main(String[] args){
		Demo obj1=new Demo("Core2web");
		Demo obj2=new Demo("Biencaps");

		WeakHashMap hm=new WeakHashMap();
		hm.put(obj1,2016);
		hm.put(obj2,2019);

		System.out.println(hm);

		obj1=null;
		System.gc();
		System.out.println(hm);
	}
}
