class forEachDemo{
	public static void main(String[] args){
		int arr[]={10,20,30,40,50,60,70,80,90,100};

		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+"\t");
		}

		System.out.println();

		for(int x: arr){
			System.out.print(x+"\t");
		}

		System.out.println();

		for(float x:arr){
			System.out.print(x+"\t");
		}
		System.out.println();
	}
}
