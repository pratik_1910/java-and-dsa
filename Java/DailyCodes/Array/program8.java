class Demo{
	public static void main(String[] args){
		int x=10;   //this will be in integer cache
		int y=10;   // this will be in integer cache
		Integer z=10;  // this will be in integer cache

		System.out.println(System.identityHashCode(x));
		System.out.println(System.identityHashCode(y));
		System.out.println(System.identityHashCode(z));
	}
}
