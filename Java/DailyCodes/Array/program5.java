// print the even elemnts in an array

import java.io.*;

class Demo{
	public static void main(String[] args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the array size : ");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("Enter the array elements : ");
		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.print("The even elements in an array are : ");
		for(int i=0;i<size;i++){
			if(arr[i]%2==0){
				System.out.print(arr[i]+"\t");
			}
		}
		System.out.println();
	}
}

