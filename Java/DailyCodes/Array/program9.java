class Demo{
	public static void main(String[] args){
		int x=100;
		int y=100;
		int z=200;
		int p=200;
		System.out.println(System.identityHashCode(x));  //this will be in integer cache
		System.out.println(System.identityHashCode(y));  // this will be in integer cache
		System.out.println(System.identityHashCode(z));  // this will be on heap
		System.out.println(System.identityHashCode(p));  // this will be on heap
	}
}
