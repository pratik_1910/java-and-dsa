class Demo{
	public static void main(String[] args){
		int arr1[]={10,20,30,40,50};
		char arr2[]={'A','B','C'};
		float arr3[]={10.5f,20.5f};
		boolean arr4[]={true,false,true};
		
		System.out.print("Arr1 = ");
		for(int i=0;i<arr1.length;i++){
			System.out.print(arr1[i]+"\t");
		}
		System.out.println();

		System.out.print("Arr2 = ");
		for(int i=0;i<arr2.length;i++){
			System.out.print(arr2[i]+"\t");
		}
		System.out.println();

		System.out.print("Arr3 = ");
		for(int i=0;i<arr3.length;i++){
			System.out.print(arr3[i]+"\t");
		}
		System.out.println();

		System.out.print("Arr4 = ");
		for(int i=0;i<arr4.length;i++){
			System.out.print(arr4[i]+"\t");
		}
		System.out.println();
	}
}
