//  In class there could be only one constructor with the unique parameters ,if we write more than one constructor with the same parameters  in the same class then the method signature of both the constructor methods is 
//  same in the method table of the class


class Demo{
	int x=10;
	Demo(){
		System.out.println("In constructor 1");
	}
	
	Demo(){
		System.out.println("in constructor 2");
	}
}
