class Demo{
	int x=10;
	Demo(){
		System.out.println("in no-args constructor");
	}
	Demo(int x){
		System.out.println("In the para constructor");
	}
	public static void main(String[] args){
		Demo obj1= new Demo();
		Demo obj=new Demo(10);
	}
}
