class Demo{
	public static void main(String[] args){
		int x=5;
		switch(x){
			case 1:
				System.out.println("One");
				break;

			case 2:
				System.out.println("first-Two");
				break;

			case 5:
				System.out.println("first-Five");
				break;

			case 5:
				System.out.println("second-Five");
				break;

			case 2:
				System.out.println("second-Two");
				break;

			default:
				System.out.println("No Match");
				break;
		}
		System.out.println("After switch");
	}
}
