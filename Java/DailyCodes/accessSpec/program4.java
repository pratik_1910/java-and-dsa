class Demo{
	int count=10;
	protected String str="Core2web";

	protected void fun(){
		System.out.println(count);
		System.out.println(str);
	}
}

class Home{
	public static void main(String[] args){
		Demo obj=new Demo();
		obj.fun();
		System.out.println(obj.count);
		System.out.println(obj.str);
	}
}
