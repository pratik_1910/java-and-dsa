class Demo{
	int count=100;
	String str="Access specifier";

	private void fun(){
		System.out.println(count);
		System.out.println(str);
	}
}

class Home{
	public static void main(String[] args){
		Demo obj=new Demo();
		//obj.fun();
		System.out.println(obj.count);
		System.out.println(obj.str);
	}
}
