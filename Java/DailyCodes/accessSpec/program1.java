// If the access specifier is not given then it is default access specifier

class Core2Web{
	int noOfRows=8;
	String favCourse="C.Cpp.Dsa";

	void display(){
		System.out.println(noOfRows);
		System.out.println(favCourse);
	}
}

class Demo{
	public static void main(String[] args){
		Core2Web obj=new Core2Web();
		obj.display();
	}
}
