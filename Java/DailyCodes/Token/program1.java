import java.util.*;
import java.io.*;

class TokenDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter student name,RollNo,percentage,division ");
		String info = br.readLine();
		System.out.println(info);
		StringTokenizer obj=new StringTokenizer(info,",");

		String token1=obj.nextToken();
		String token2= obj.nextToken();
		String token3=obj.nextToken();
		String token4=obj.nextToken();

		System.out.println("Name :"+token1);
		System.out.println("RollNo ="+token2);
		System.out.println("Percentage ="+token3);
		System.out.println("Division :"+token4);
	}
}
