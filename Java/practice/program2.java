class Matter{
	Matter(){
		System.out.println("In Matter Constructor");
	}
	void Features(){
		System.out.println("Properties : Volume , Boiling Point , Melting point , Density , Texture ");
	}
}

class States extends Matter{
	States(){
		System.out.println("In States constructor");
	}
	void Phases(){
		System.out.println("States : Solid , Liquid , Gas , Plasma ");
	}
}
class Client{
	public static void main(String[] args){
		States obj=new States();
		obj.Features();
		obj.Phases();
	}
}

