class Universe{
	Universe(){
		System.out.println("In unuverse constructor");
	}
	void fun(){
		System.out.println("galaxy");
	}
}
class Milkyway extends Universe{
	Milkyway(){
		System.out.println("In Milkyway constructor");
	}
}
class Earth extends Milkyway{
	Earth(){
		System.out.println("In Earth Constructor");
	}
}
class Client{
	public static void main(String[] args){
		Universe obj=new Earth();
		obj.fun();
	}
}

