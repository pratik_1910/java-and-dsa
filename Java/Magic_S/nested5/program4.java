// take a range and print even numbers in reverse order and odd numbers in standard format

import java.io.*;

class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the start of the range");
		int start = Integer.parseInt(br.readLine());

		System.out.println("Enter the end of the range");
		int end= Integer.parseInt(br.readLine());
		
		System.out.print("Odd numbers from the range are : ");
		for(int i=start;i<=end;i++){
			if(i%2==1){
				System.out.print(i+"  ");
			}
		}
		
		System.out.println();
		System.out.print("Even numbers from the range are: ");
		for(int j=end;j>=start;j--){
			if(j%2==0){
				System.out.print(j+"  ");
			}
		}
		System.out.println();
	}
}

