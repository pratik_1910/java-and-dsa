// for rows=5;                                 for rows=4;

//  O                                          10 
//  14   13                                    I    H
//  L    k    J                                7    6    5    
//  9    8    7    6                           D    C    B    A
//  E    D    C    B    A

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number of rows : ");
		int row=Integer.parseInt(br.readLine());
		System.out.println();
		
		int n=(row*(row+1))/2;
		int ch=64+n;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				if(row%2==0){
					if(i%2==0){
						System.out.print((char)ch+"\t");
					}
					else{
						System.out.print(n+"\t");
					}
				}else{
					if(i%2==0){
						System.out.print(n+"\t");
					}
					else{
						System.out.print((char)ch+"\t");
					}
				}
				ch--;
				n--;
			}
			System.out.println();
		}
	}
}


