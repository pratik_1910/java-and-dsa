//    D4  C3  B2  A1
//    A1  B2  C3  D4
//    D4  C3  B2  A1
//    A1  B2  C3  D4

import java.io.*;

class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number of rows");
		int row=Integer.parseInt(br.readLine());
		
		for(int i=1;i<=row;i++){
			int ch1=(65-1+row);
			int n1=row,ch2=65,n2=1;
			for(int j=1;j<=row;j++){
				if(i%2==1){
					System.out.print((char)ch1+""+(n1)+" ");
					ch1--;
					n1--;
				}
				else{
					System.out.print((char)ch2+""+(n2)+" ");
					ch2++;
					n2++;
				}
			}
			System.out.println();
		}
	}
}

