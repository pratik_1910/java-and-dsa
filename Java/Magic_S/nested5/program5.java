// 0
// 1   1
// 2   3    5
// 8   13   21   34

import java.io.*;

class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number of rows");
		int row=Integer.parseInt(br.readLine());
		
		System.out.println();
		int x=0,y=1,z=0;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				z=x+y;
				System.out.print(x+" ");
				x=y;
				y=z;
			}
			System.out.println();
		}
	}
}

