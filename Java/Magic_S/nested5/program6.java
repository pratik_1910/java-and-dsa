//Take two alphabets and find difference between their ascii values

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the first alphabet :");
		char alpha1=br.readLine().charAt(0);

		System.out.println("Enter the secong alphabet");
		char alpha2=br.readLine().charAt(0);
		
		int diff=0;
		if((int)alpha1 > (int)alpha2){
			diff=(int)alpha1 -(int)alpha2;
		}else{
			diff=(int)alpha2-(int)alpha1;
		}

		System.out.println("The difference between ASCII values of "+alpha1+" and "+alpha2+" is : "+diff);

	}
}

