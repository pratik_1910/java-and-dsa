//take an intput as number and print the addition of factorials of each digit from that number

import java.io.*;

class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number : ");
		int num=Integer.parseInt(br.readLine());
		System.out.println();
	
		int temp=num;
		int rem=0,sum=0;
		while(num!=0){
			int fact=1;
			rem=num%10;
			for(int i=1;i<=rem;i++){
				fact=fact*i;
			}
			sum=sum+fact;
			num=num/10;
		}

		System.out.println("The sum of the factorials of the digits from "+temp+" is : "+sum);
	}
}


			


