// Write a program to print sum of even numbers and multiplication of odd numbers between 1 to 10

class Demo{
	public static void main(String[] args){
		int i =1;
		int N=10;
		int sum=0,mult=1;
		while(i<=N){
			if(i%2==0){
				sum=sum+i;
			}else{
				mult=mult*i;
			}
			i++;
		}	
		System.out.println("sum of even numbers = "+sum);
		System.out.println("Multiplication of odd numbers = "+mult);
		
	}
}

