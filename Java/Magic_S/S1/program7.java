// if the given no. is even print it in reverse order and if the no. is odd the print it in reverse order by difference of 2

class Demo{
	public static void main(String[] args){
		int N=9;

		if(N%2==0){
			while(N!=0){
				System.out.println(N--);
			}
		}else{
			while(N>=0){
				System.out.println(N);
				N=N-2;
			}
		}
	}
}

