//write a program to count digits of the gien number

class Demo{
	public static void main(String[] args){
		int N=1234;
		int count=0;
		while(N!=0){
			count++;
			N=N/10;
		}
		System.out.println("No. of digits is = "+count);
	}
}
