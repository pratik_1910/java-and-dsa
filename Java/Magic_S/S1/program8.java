// Write a program to print countdown of days to submit an assignment

class Demo{
	public static void main(String[] args){
		int days =7;

		if(days>0){
			while(days!=0){

				System.out.println(days+" days remaining");
				days--;
			}
		}else if(days==0){
			System.out.println("0 days assignment is overdue ");
		}else{
			System.out.println("Invalid input");
		}
	}
}

