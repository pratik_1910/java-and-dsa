//count no. of odd digits in a number

class Demo{
	public static void main(String[] args){
		int N=1234;
		int count=0;
		int rem=0;

		while(N!=0){
			rem = N%10;
			if(rem%2!=0){
				count++;
			}
			N=N/10;
		}
		System.out.println(count);
	}
}

