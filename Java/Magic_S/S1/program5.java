//write a program to print the square of even digits of the given number

class Demo{
	public static void main(String[] args){
		int N=1234;
		int rem =0;
		while(N!=0){
			rem=N%10;
			if(rem%2==0){
				System.out.println(rem*rem);
			}
			N=N/10;
		}
	}
}
