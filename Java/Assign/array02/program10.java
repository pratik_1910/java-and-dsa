//print the elements from the array whose addition of digits is even

import java.util.*;

class Demo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter the array size : ");
		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter the array elements : ");
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}

		System.out.print("elements whose addition of digits is even : ");
		for(int i=0;i<size;i++){
			int sum=0;
			int temp=arr[i];
			while(arr[i]!=0){
				int rem=arr[i]%10;
				sum =sum+rem;
				arr[i]=arr[i]/10;
			}
			if(sum%2==0){
				System.out.print(temp+"\t");
			}
		}
		System.out.println();
	}
}
