import java.util.*;

class Demo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the array size : ");
		int size=sc.nextInt();
		int arr[]=new int[size];

		int sum=0;
		System.out.println("Enter the array elements : ");
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
			sum=sum+arr[i];
		}
		System.out.println("The addition of the array elements = "+sum);
	}
}
		


