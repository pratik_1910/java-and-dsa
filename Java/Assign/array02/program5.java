import java.util.*;

class Demo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the size of the array : ");
		int size=sc.nextInt();
		int arr[]=new int[size];

		System.out.println("Enter the array elements : ");
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}

		int min=arr[0];
		for(int i=1;i<size;i++){
			if(min>arr[i]){
				min=arr[i];
			}
		}
		System.out.println("Minimum element : "+min);
	}
}


