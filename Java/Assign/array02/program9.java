import java.util.*;

class Demo{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the first array size : ");
		int size1=sc.nextInt();
		int arr1[]=new int[size1];
		System.out.println("Enter the first array elements : ");
		for(int i=0;i<size1;i++){
			arr1[i]=sc.nextInt();
		}

		System.out.println("Enter the second array size : ");
		int size2=sc.nextInt();
		int arr2[]=new int[size2];
		System.out.println("Enter the secod array elements : ");
		for(int i=0;i<size2;i++){
			arr2[i]=sc.nextInt();
		}

		int size3=size1+size2;
		int arr3[]=new int[size3];
		for(int i=0;i<size1;i++){
			arr3[i]=arr1[i];
		}
		for(int i=size1;i<size3;i++){
			arr3[i]=arr2[i-size1];
		}

		System.out.print("Merged array = { ");
		for(int i=0;i<size3;i++){
			System.out.print(arr3[i]+" , ");
		}
		System.out.println("}");
	}
}
