import java.util.*;

class Demo{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the size of the array : ");
		int size= sc.nextInt();
		int arr[]=new int[size];

		int evensum=0,oddsum=0;
		System.out.println("Enter the array elements : ");
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
			if(arr[i]%2==0){
				evensum=evensum+arr[i];
			}
			else{
				oddsum=oddsum+arr[i];
			}
		}

		System.out.println("odd numbers sum = "+oddsum);
		System.out.println("Even numbers sum = "+evensum);
	}
}


