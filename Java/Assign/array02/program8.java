import java.util.*;

class Demo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the first array size : ");
		int size1=sc.nextInt();
		int arr1[]=new int[size1];
		System.out.println("Enter the first array elements : ");
		for(int i=0;i<size1;i++){
			arr1[i]=sc.nextInt();
		}

		System.out.println("Enter the second array size : ");
		int size2=sc.nextInt();
		int arr2[]=new int[size2];
		System.out.println("Enter the second array elements : ");
		for(int i=0;i<size2;i++){
			arr2[i]=sc.nextInt();
		}
		
		System.out.println("The uncommon elements are : ");
		for(int i=0;i<size1;i++){
			int count1=0;
			for(int j=0;j<size2;j++){
				if(arr1[i]==arr2[j]){
					count1++;
				}
			}
			if(count1==0){
				System.out.println(arr1[i]);
			}
		}

		for(int i=0;i<size2;i++){
			int count2=0;
			for(int j=0;j<size1;j++){
				if(arr2[i]==arr1[j]){
					count2++;
				}
			}
			if(count2==0){
				System.out.println(arr2[i]);
			}
		}
	}
}

