//  write a program to print count of digits in elements in an array

import java.util.*;
class Demo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the array size : ");
		int size=sc.nextInt();
		int arr[]= new int[size];
		System.out.println("Enter the array elements : ");
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		
		System.out.print("Output : ");
		for(int i=0;i<size;i++){
			int count=0;
			while(arr[i]!=0){
				count++;
				arr[i]=arr[i]/10;
			}
			System.out.print(count+"\t");
		}
		System.out.println();
	}
}
