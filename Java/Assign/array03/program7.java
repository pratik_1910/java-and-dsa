//  write a program to find strong number and print its index

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the array size : ");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		System.out.println("Enter the array elements : ");
		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		for(int i=0;i<size;i++){
			int sum=0,temp=arr[i];
			int rem=0;
			while(arr[i]!=0){
				rem=arr[i]%10;
				int fact=1;
				for(int j=1;j<=rem;j++){
					fact=fact*j;
				}
				sum=sum+fact;
				arr[i]=arr[i]/10;
			}
			if(temp==sum){
				System.out.println("The strong number "+temp+" is found at "+i);
			}
		}
	}
}
			

