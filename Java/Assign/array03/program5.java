//  write  program to find the perfect no. from an array and print its index

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the array size : ");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		System.out.println("Enter the array elements : ");
		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		for(int i=0;i<size;i++){
			int temp=arr[i];
			int sum=0;
			for(int j=1;j<arr[i];j++){
				if(arr[i]%j==0){
					sum=sum+j;
				}
			}
			if(sum==temp){
				System.out.println("perfect no. "+arr[i]+" found at  "+i);
			}
		}
	}
}

