//  write a program to find the armstrong numbers and print their index

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the array size : ");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		System.out.println("Enter the array elements : ");
		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		for(int i=0;i<size;i++){
			int temp=arr[i];
			int count=0;
			int sum=0;
			while(arr[i]!=0){
				count++;
				arr[i]=arr[i]/10;
			}

			
			for(int k=temp;k!=0;k=k/10){
				int prod=1;
				int rem=k%10;
				for(int j=1;j<=count;j++){
					prod=prod*rem;
				}
				sum=sum+prod;
			}
			if(sum==temp){
				System.out.println("armstrong number "+temp+" is found at "+i);
			}
		}
	}
}
