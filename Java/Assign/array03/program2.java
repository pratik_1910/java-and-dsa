// write a program to reverse each element in an array

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Entert he array size : ");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		System.out.println("Enter the array elements : ");
		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.print("Output : ");
		for(int i=0;i<size;i++){
			int rev=0,rem=0;
			while(arr[i]!=0){
				rem=arr[i]%10;
				rev=(rev*10)+rem;
				arr[i]=arr[i]/10;
			}
			System.out.print(rev+"\t");
		}
		System.out.println();
	}
}

