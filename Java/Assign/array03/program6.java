//  write a program to find pallindrome no. and print its index

import java.io.*;
class Demo{
	public static void main(String[] main)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the array size : ");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		System.out.println("Enter the array elements : ");
		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		for(int i=0;i<size;i++){
			int temp=arr[i];
			int rem=0,rev=0;
			while(arr[i]!=0){
				rem=arr[i]%10;
				rev=(rev*10)+rem;
				arr[i]=arr[i]/10;
			}
			if(rev==temp){
				System.out.println("pallindrome no. "+temp+" is found at "+i);
			}
		}
	}
}
