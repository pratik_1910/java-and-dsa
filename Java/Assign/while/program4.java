//  program to count the odd digits in the number

class Demo{
	public static void main(String[] args){
		int count=0,rem=0;
		int n=1234;

		while(n!=0){
			rem=n%10;
			if(rem%2==1){
				count++;
			}
			n=n/10;
		}
		System.out.println("Number of odd digits in the given number : "+count);
	}
}
