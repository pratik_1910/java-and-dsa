//  print the square of the even digits of the given number

class Demo{
	public static void main(String[] args){
		int count=0,rem=0;
		int n=1234;

		while(n!=0){
			rem=n%10;
			if(n%2==0){
				System.out.println(rem*rem);
			}
			n=n/10;
		}
	}
}
