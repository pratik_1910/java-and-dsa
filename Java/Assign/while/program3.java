//  Write a program to count the digits of the given number

class Demo{
	public static void main(String[] args){
		int rem=0,count=0;
		int n=1234;

		while(n!=0){
			rem=n%10;
			count++;
			n=n/10;
		}
		System.out.println("The number of digits is : "+count);
	}
}
