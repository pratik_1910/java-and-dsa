//  write a program to print the sum of even numbers and multiplication of odd numbers in 1 to 10

class Demo{
	public static void main(String[] args){
		int sum=0,mult=1;
		int i=1;

		while(i<=10){
			if(i%2==0){
				sum=sum+i;
			}else{
				mult=mult*i;
			}
			i++;
		}
		System.out.println("The sum of even numbers between 1 to 10 is : "+sum);
		System.out.println("The multiplication of the odd numbers between 1 to 10 is : "+mult);
	}
}
