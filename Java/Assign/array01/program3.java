// product of the elements whose index is odd 

import java.util.*;
class Demo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the size of the array : ");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter the array elements : ");
		int mult=1;
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
			if(i%2==1){
				mult=mult*arr[i];
			}
		}
		System.out.println("The product of the elements at the odd index is : "+mult);
	}
}
