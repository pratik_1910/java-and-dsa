//   print the elements from the array which are divided by 5

import java.util.*;

class Demo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the array size : ");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter the array elements : ");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		System.out.print("The array elements which are divided by 5 are : ");
		for(int i=0;i<arr.length;i++){
			if(arr[i]%5==0){
				System.out.print(arr[i]+"\t");
			}
		}
		System.out.println();
	}
}
				

