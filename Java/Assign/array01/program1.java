// print the sum of odd elements in an array

import java.io.*;

class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the size of the array : ");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		System.out.println("Eneter the array elements : ");

		int sum=0;
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==1){
				sum=sum+arr[i];
			}
		}

		System.out.println("The sum of odd elements in an array is : "+sum);
	}
}


