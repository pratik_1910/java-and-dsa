// take input string from user and print its number of vowels and consonent in the given string

import java.io.*;
class Demo{
	static void calAlphabet(String str){
		int length=str.length();
		int temp=0;
		int cCount=0,vCount=0;
		while(temp<length){
			if((str.charAt(temp)=='a')||(str.charAt(temp)=='e')||(str.charAt(temp)=='i')||(str.charAt(temp)=='o')||
					(str.charAt(temp)=='u')||(str.charAt(temp)=='A')||(str.charAt(temp)=='E')||
					(str.charAt(temp)=='I')||(str.charAt(temp)=='O')||(str.charAt(temp)=='U')){
				vCount++;
			}
			else{
				cCount++;
			}
			temp++;
		}
		System.out.println("The vowels count = "+vCount);
		System.out.println("The consonent count = "+cCount);
	}

	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the string : ");
		String str=br.readLine();

		calAlphabet(str);
	}
}

