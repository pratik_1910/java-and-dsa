// write a program to print the pattern 
// row=3
//           *
//           *  *
//           *  *  *
//           *  *
//           *

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the row size : ");
		int row=Integer.parseInt(br.readLine());
		int var=1;

		for(int i=1;i<=(2*row)-1;i++){
			if(i<=row){

				for(int j=0;j<i;j++){
					System.out.print("*\t");
				}
			}else{
				for(int j=0;j<i-2*var;j++){
					System.out.print("*\t");
				}
				var++;
			}
			System.out.println();
		}
	}
}
					

