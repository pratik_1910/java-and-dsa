// writw a program to take 2 input string from the user and check that they are anagram or not

import java.io.*;
class Demo{
	static boolean isAnagram(String str1,String str2){
		if(str1.length()!=str2.length()){
			return false;
		}
		else{
			char arr1[]=str1.toCharArray();
			char arr2[]=str2.toCharArray();
			for(int i=0;i<arr1.length;i++){
				char temp=arr1[i];
				int count1=0,count2=0;
				for(int j=0;j<arr1.length;j++){
					if(temp==arr1[j]){
						count1++;
					}
				}
				for(int j=0;j<arr2.length;j++){	
					if(temp==arr2[j]){
						count2++;
					}
				}
				if(count1!=count2){
					return false;
				}

			}
			return true;
		}
	}
	public static void main(String[] args)throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the first string : ");
		String str1=br.readLine();
		System.out.println("Enter the second string : ");
		String str2=br.readLine();

		if(isAnagram(str1,str2)){
			System.out.println(str1 + " and "+str2+" are anagram strings ");
		}
		else{
			System.out.println(str1+" and "+str2+ " are not anagram strings ");
		}
	}
}	




