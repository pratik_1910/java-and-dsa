// write a program to take input from user to check whether given string is pallindrome  string or not

import java.io.*;
class Demo{
	static int lenString(String str){
		int count=0;
		char ch[]=str.toCharArray();
		for(char a : ch){
			count++;
		}
		return count;
	}

	static boolean ispal(String str){
		int length=lenString(str);

		int start=0;
		int end=length-1;

		while(start<end){
			if(str.charAt(start) != str.charAt(end)){
				return false;
			}
			else{
				start++;
				end--;
			}
		}
		return true;
	}

	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the string to be checked : ");
		String str=br.readLine();

		if(ispal(str)){
			System.out.println(str+" is pallindrome string ");
		}
		else{
			System.out.println(str+" is not a pallindrome string ");
		}
	}
}

		

