// take input from user in 2D and print array by skip of the diagonal  elements

import java.util.*;
class Demo{
	static void printarray(int arr[][]){
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				if(i==j){
					continue;
				}
				System.out.print(arr[i][j]+"\t");
			}
		}
	}
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the size of 2D array : ");
		System.out.println("Enter the row size : ");
		int row=sc.nextInt();
		System.out.println("Enter the column size : ");
		int col=sc.nextInt();
		int arr[][]=new int[row][col];

		System.out.println("Enter the array elements : ");
		for(int i=0;i<row;i++){
			for(int j=0;j<col;j++){
				arr[i][j]=sc.nextInt();
			}
		}
		printarray(arr);
	}
}




