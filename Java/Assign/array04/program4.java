// wtrite a program to print the squareroot of the numbers in  given range

import java.io.*;
class Demo{
	static void sqRoot(int start,int end){
		for(int i=start;i*i<=end;i++){
			System.out.print((i*i)+"\t");
		}
		System.out.println();
	}

	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the start of the range : ");
		int start =Integer.parseInt(br.readLine());
		System.out.println("Enter the end of the range : ");
		int end=Integer.parseInt(br.readLine());
	
		sqRoot(start,end);
	}
}	
			
